#include "Unit.h"

Unit::Unit(const std::string& name) : name(name) {
    this->state = new State();
}

Unit::~Unit() {
    delete this->state;
}

const std::string& Unit::getName() const {
    return name;
}

State* Unit::getState() const {
    return state;
}

Ability* Unit::getAbility() const {
    return ability;
}

void Unit::changeAbility(Ability* newAbility) {
    this->ability = newAbility;
}

void Unit::takeDamage(int dmg) {
    getState()->isAlive();
    if ( dmg >= getState()->hp ) {
        notify();
        getState()->hp = 0;
        return;
    }
    getState()->hp -= dmg;
}

void Unit::takeMagicDamage(int dmg) {
    if ( this->getState()->isWerewolf ) {
        takeDamage(dmg * 2);
    } else {
        takeDamage(dmg);
    }
}

void Unit::attack(Unit* enemy) {
    this->ability->action(enemy);
}

void Unit::notify() {
    std::set<Observer*>::iterator it = observers.begin();

    for ( ; it != observers.end(); it++ ) {
        ((Unit*)(*it))->getState()->addHp(((Unit*)(*it))->getState()->hpLimit/10);
        (*it)->removeObservable(this);
    }
}

void Unit::sendNotification() {
    std::set<Observable*>::iterator it = observables.begin();

    for ( ; it != observables.end(); it++ ) {
        (*it)->removeObserver(this);
    }
}

std::ostream& operator<<(std::ostream& out, const Unit& unit) {
    out << unit.getName();
    out << *unit.getState();
    out << "**Ability: " << '\n';
    out << "--" << unit.getAbility()->getType() << std::endl;
    return out;
}
