#include "Werewolf.h"

Werewolf::Werewolf(const std::string& name) : Unit(name) {
	this->ability = new DefaultAttack(this, "Basic Melee Attack");
	this->getState()->isWerewolf = false;

	this->altAbility = new WerewolfRage(this, "Wolverine Claws");

	current = new State(100, 20, 0);
	alternative = new State(150, 40, 0);

	this->getState()->damage = current->damage;
	this->getState()->hp = current->hp;
	this->getState()->hpLimit = current->hpLimit;
}

Werewolf::~Werewolf() {}

void Werewolf::changeState() {
	alternative->hp = this->getState()->hp * alternative->hpLimit / current->hp;

	State* tmpState = current;
    current = alternative;
    alternative = tmpState;

    this->getState()->hp = current->hp;
    this->getState()->hpLimit = current->hpLimit;
    this->getState()->damage = current->damage;

    Ability* tmpAbility = this->ability;
    this->ability = altAbility;
    altAbility = tmpAbility;

    if ( this->getState()->isWerewolf ) {
    	this->getState()->isWerewolf = false;
    } else {
    	this->getState()->isWerewolf = true;
    }
}