#ifndef UNIT_H
#define UNIT_H

#include <iostream>
#include "Ability.h"
#include "State.h"
#include "Observer.h"
#include "Observable.h"

class Ability;

class Unit : public Observer, public Observable {
    protected:
        std::string name;
        Ability* ability;
        State* state;

    public:
        Unit(const std::string& name="Unit");
        ~Unit();

        const std::string& getName() const;
        Ability* getAbility() const;
        State* getState() const;

        void changeAbility(Ability* newAbility);

        virtual void takeMagicDamage(int dmg);
        virtual void takeDamage(int dmg);

        virtual void attack(Unit* enemy);

        virtual void notify();
        virtual void sendNotification();

};

std::ostream& operator<<(std::ostream& out, const Unit& unit);

#endif //UNIT_H