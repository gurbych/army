#ifndef DEFAULT_ATTACK_H
#define DEFAULT_ATTACK_H

#include "Ability.h"

class DefaultAttack : public Ability {
	public:
		DefaultAttack(Unit* owner, const std::string& type);
		virtual ~DefaultAttack();

		virtual void action(Unit* enemy);
};

#endif