#include "RogueAttack.h"

RogueAttack::RogueAttack(Unit* owner, const std::string& type) : Ability(owner, type) {}

RogueAttack::~RogueAttack() {
	delete owner;
}

void RogueAttack::action(Unit* enemy) {
	this->owner->getState()->isAlive();
	enemy->takeDamage(this->owner->getState()->damage);
}