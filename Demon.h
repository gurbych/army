#ifndef DEMON_H
#define DEMON_H

#include "Unit.h"
#include "DefaultAttack.h"
#include "Warlock.h"

class Warlock;

class Demon : public Unit {
	private:
        Warlock* master;

	public:
        Demon(const std::string& name, Warlock* master);
        virtual ~Demon();
};

#endif