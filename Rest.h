#ifndef REST_H
#define REST_H

#include "Spell.h"

class Rest : public Spell {
    public:
        Rest(const std::string& spellName, Unit* spellcaster);
        virtual ~Rest();

        void action(Unit* target);
};

#endif
