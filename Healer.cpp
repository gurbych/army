#include "Healer.h"

Healer::Healer(const std::string& name) : Spellcaster(name) {
	this->getState()->isCombatMagician = false;
	this->getState()->isPriest = false;
}

Healer::~Healer() {}