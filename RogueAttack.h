#ifndef ROGUE_ATTACK_H
#define ROGUE_ATTACK_H

#include "Ability.h"

class RogueAttack : public Ability {
	public:
		RogueAttack(Unit* owner, const std::string& type);
		virtual ~RogueAttack();

		virtual void action(Unit* enemy);
};

#endif