#include "Priest.h"

Priest::Priest(const std::string& name) : Spellcaster(name) {
	this->getState()->isCombatMagician = false;
	this->getState()->isPriest = true;
}

Priest::~Priest() {}