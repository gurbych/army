#include "Spell.h"

int Spell::spellCost = 30;
int Spell::spellPower = 30;

Spell::Spell(const std::string& spellName, Unit* spellcaster) {
    this->spellName = spellName;
    this->spellcaster = spellcaster;
}

Spell::~Spell() {}

const std::string& Spell::getSpellName() const {
    return spellName;
}
