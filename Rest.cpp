#include "Rest.h"

Rest::Rest(const std::string& spellName, Unit* spellcaster) : Spell(spellName, spellcaster) {}

Rest::~Rest() {}

void Rest::action(Unit* target) {
	if ( this->spellcaster->getState()->isCombatMagician ) {
		target->getState()->addMana(spellPower / 2);
	} else if ( !target->getState()->alive && this->spellcaster->getState()->isPriest ) {
		target->takeMagicDamage(spellPower * 2);
	} else {
		target->getState()->addMana(spellPower);
	}
}
