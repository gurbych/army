#ifndef WEREWOLF_H
#define WEREWOLF_H

#include "Unit.h"
#include "WerewolfRage.h"
#include "DefaultAttack.h"

class Werewolf : public Unit {
	private:
		Ability* altAbility;
		State* current;
		State* alternative;

	public:
        Werewolf(const std::string& name="Werewolf");
        virtual ~Werewolf();

        void changeState();
};

#endif