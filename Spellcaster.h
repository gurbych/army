#ifndef SPELLCASTER_H
#define SPELLCASTER_H

#include "Unit.h"
#include "DefaultAttack.h"
#include "Fireball.h"
#include "Heal.h"
#include "Rest.h"
#include <map>

class Spellcaster : public Unit {
protected:
        std::map<std::string, Spell*>* spellbook;

public:
	Spellcaster(const std::string& name="Spellcaster");
	virtual ~Spellcaster();

	void addMana(int extra);
        void spendMana(int cost);

        std::map<std::string, Spell*>& getSpellbook() const;

        void addSpell(const std::string& spellName, Spell* newSpell);
        void removeSpell(const std::string& oldSpell);

        virtual void cast(const std::string& spellName, Unit* target);
};

std::ostream& operator<<(std::ostream& out, const Spellcaster& sc);

#endif