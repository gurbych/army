#include "Warlock.h"

Warlock::Warlock(const std::string& name) : Spellcaster(name) {
	this->getState()->isCombatMagician = true;
	this->getState()->isPriest = false;

	slave = NULL;
}

Warlock::~Warlock() {
	if ( slave != NULL ) {
        freeSlave();
    }
}

void Warlock::setSlave(Demon* demon) {
    slave = demon;
}

void Warlock::summon() {
    spendMana(50);
    if ( slave != NULL ) {
        freeSlave();
    }
    slave = new Demon("Kakodaimonos", this);
}

void Warlock::freeSlave() {
    delete slave;
    slave = NULL;
}

Demon* Warlock::demon() {
    if ( slave != NULL ) {
        return slave;
    }
}