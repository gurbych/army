#include "State.h"

State::State(int hp, int damage, int mana) {
    this->hp = hp;
    this->hpLimit = hp;
    this->damage = damage;
    this->mana = mana;
    this->manaLimit = mana;
    this->alive = true;
    this->isWerewolf = false;
    this->isVampire = false;
}

State::~State() {}

void State::isAlive() {
    if ( hp == 0 ) {
        throw UnitIsDeadException();
    }
}

int State::getHp() const {
    return hp;
}

int State::getHpLimit() const {
    return hpLimit;
}

int State::getDamage() const {
    return damage;
}

void State::setDamage(int dmg) {
    damage = dmg;
}

void State::addHp(int hitp) {
    isAlive();
    int total = hp + hitp;

    if ( total > hpLimit ) {
            hp = hpLimit;
    } else {
            hp = total;
    }
}

void State::addMana(int extra) {
    isAlive();
    int total = mana + extra;

    if ( total > manaLimit ) {
        mana = manaLimit;
        return;
    }
  mana = total;
}

void State::turnToUndead() {
    this->alive = false;
}

std::ostream& operator<<(std::ostream& out, const State& state) {
    out << (state.alive ? " (alive):" : " (undead):") << '\n' <<"--hp       " << state.getHp() << '/' << state.getHpLimit() << '\n' <<"--mp       " << state.mana << '/' << state.manaLimit << '\n' << "--damage   " << state.getDamage() << std::endl;
    return out;
}
