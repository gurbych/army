#include "WerewolfRage.h"

WerewolfRage::WerewolfRage(Unit* owner, const std::string& type) : Ability(owner, type) {
	chanceToInfect = 1;
}

WerewolfRage::~WerewolfRage() {
	delete owner;
}

void WerewolfRage::action(Unit* enemy) {
	this->owner->getState()->isAlive();
	enemy->takeDamage(this->owner->getState()->damage);
	this->owner->takeDamage(this->owner->getState()->damage / 2);

	if ( chanceToInfect % 2 == 0 ) {
		if ( !(enemy->getState()->isVampire) ) {
			enemy->changeAbility(new WerewolfRage(enemy, "Werewolf Rage"));
		}
	}

	chanceToInfect += 1;
}