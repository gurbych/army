#include "DefaultAttack.h"

DefaultAttack::DefaultAttack(Unit* owner, const std::string& type) : Ability(owner, type) {}

DefaultAttack::~DefaultAttack() {
	delete owner;
}

void DefaultAttack::action(Unit* enemy) {
	this->owner->getState()->isAlive();
	enemy->takeDamage(this->owner->getState()->damage);
	this->owner->takeDamage(this->owner->getState()->damage / 2);
}