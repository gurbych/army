#ifndef ABILITY_H
#define ABILITY_H

#include <iostream>
#include "Unit.h"

class Unit;

class Ability {
    protected:
        Unit* owner;
        std::string type;

    public:
        Ability(Unit* owner, const std::string& type);
        virtual ~Ability();

        Unit* getOwner() const;
        const std::string& getType() const;

        virtual void action(Unit* enemy) = 0;
};


#endif //CAR_H