#include "Fireball.h"

Fireball::Fireball(const std::string& spellName, Unit* spellcaster) : Spell(spellName, spellcaster) {}

Fireball::~Fireball() {}

void Fireball::action(Unit* target) {
	if ( this->spellcaster->getState()->isCombatMagician ) {
		target->takeMagicDamage(spellPower);
	} else if ( !target->getState()->alive && this->spellcaster->getState()->isPriest ) {
		target->takeMagicDamage(spellPower * 2);
	} else {
		target->takeMagicDamage(spellPower / 2);
	}
}
