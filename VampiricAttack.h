#ifndef VAMPIRIC_ATTACK_H
#define VAMPIRIC_ATTACK_H

#include "Ability.h"

class VampiricAttack : public Ability {
	private:
		int chanceToInfect;

	public:
		VampiricAttack(Unit* owner, const std::string& type);
		virtual ~VampiricAttack();

		virtual void action(Unit* enemy);
};

#endif