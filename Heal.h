#ifndef HEAL_H
#define HEAL_H

#include "Spell.h"

class Heal : public Spell {
    public:
        Heal(const std::string& spellName, Unit* spellcaster);
        virtual ~Heal();

        void action(Unit* target);
};

#endif
