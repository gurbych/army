#ifndef STATE_H
#define STATE_H

#include <iostream>
#include "Exceptions.h"

class State {
    public:
        double hp;
        double hpLimit;
        int damage;
        bool alive;
        bool isVampire;
        bool isWerewolf;;
        bool isPriest;

        int mana;
        int manaLimit;
        bool isCombatMagician;
        
        State(int hp=100, int damage=30, int mana=0);
        virtual ~State();

        void isAlive();
        
        int getHp() const;
        int getHpLimit() const;
        int getDamage() const;

        virtual void addHp(int hp);
        virtual void addMana(int extra);
        virtual void setDamage(int dmg);

        void turnToUndead();
};

std::ostream& operator<<(std::ostream& out, const State& state);

#endif // STATE_H
