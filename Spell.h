#ifndef SPELL_H
#define SPELL_H

#include "Unit.h"

class Spell {
	public:
		Unit* spellcaster;

        static int spellCost;
        static int spellPower;

        std::string spellName;

        Spell(const std::string& spellName, Unit* spellcaster);
        ~Spell();

        const std::string& getSpellName() const;

        virtual void action(Unit* target) = 0;
};


#endif //Spell_H