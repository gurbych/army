#include "Spellcaster.h"

Spellcaster::Spellcaster(const std::string& name) {
	this->name = name;
	this->state = new State(50, 10, 100);
	this->ability = new DefaultAttack(this, "Basic Melee Attack");

	spellbook = new std::map<std::string, Spell*>();

	addSpell("Fireball", new Fireball("Fireball", this));
	addSpell("Heal", new Heal("Heal", this));
	addSpell("Rest", new Rest("Rest", this));
}

Spellcaster::~Spellcaster() {}

void Spellcaster::addMana(int extra) {
    this->getState()->isAlive();
    int total = this->getState()->mana + extra;

    if ( total > this->getState()->manaLimit ) {
        this->getState()->mana = this->getState()->manaLimit;
        return;
    }
  	this->getState()->mana = total;
}

void Spellcaster::spendMana(int cost) {
    this->getState()->isAlive();

    if ( cost > this->getState()->mana ) {
        throw OutOfManaException();
    }
    this->getState()->mana -= cost;
}

void Spellcaster::addSpell(const std::string& spellName, Spell* newSpell) {
	spellbook->insert(std::pair<std::string, Spell*>(spellName, newSpell));
}

void Spellcaster::removeSpell(const std::string& oldSpell) {
	spellbook->erase(oldSpell);
}

std::map<std::string, Spell*>& Spellcaster::getSpellbook() const {
	return *spellbook;
}

void Spellcaster::cast(const std::string& spellName, Unit* target) {
	this->getState()->isAlive();
	if ( spellbook->find(spellName) == spellbook->end() ) {
		throw NoSuchSpellException();
	}
	spendMana((*spellbook)[spellName]->spellCost);
	(*spellbook)[spellName]->action(target);
}

std::ostream& operator<<(std::ostream& out, const Spellcaster& sc) {
	out << (const Unit&)sc;
	out << "**Spellbook: \n";

	std::map<std::string, Spell*>::iterator it;

    for ( it = sc.getSpellbook().begin(); it != sc.getSpellbook().end(); it++ ) {
        out << "Has spell => " << it->first << std::endl;
    }

	return out;
}