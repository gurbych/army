#include "VampiricAttack.h"

VampiricAttack::VampiricAttack(Unit* owner, const std::string& type) : Ability(owner, type) {
	chanceToInfect = 1;
}

VampiricAttack::~VampiricAttack() {
	delete owner;
}

void VampiricAttack::action(Unit* enemy) {
	this->owner->getState()->isAlive();
	enemy->takeDamage(this->owner->getState()->damage);
	this->owner->takeDamage(this->owner->getState()->damage / 2);
	this->owner->getState()->addHp(this->owner->getState()->damage / 3);

	if ( chanceToInfect % 2 == 0 ) {
		if ( !(enemy->getState()->isWerewolf) ) {
			enemy->changeAbility(new VampiricAttack(enemy, "Vampirism"));
			enemy->getState()->turnToUndead();
		}
	}

	chanceToInfect += 1;
}