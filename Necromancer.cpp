#include "Necromancer.h"

Necromancer::Necromancer(const std::string& name) : Spellcaster(name) {
	this->getState()->isCombatMagician = true;
	this->getState()->isPriest = false;
	this->getState()->alive = false;
}

Necromancer::~Necromancer() {}

void Necromancer::attack(Unit* enemy) {
    this->ability->action(enemy);
    addObservable(enemy);
}

void Necromancer::takeDamage(int dmg) {
	getState()->isAlive();
    if ( dmg >= getState()->hp ) {
        notify();
        sendNotification();
        getState()->hp = 0;
        return;
    }
    getState()->hp -= dmg;
}

void Necromancer::cast(const std::string& spellName, Unit* target) {
	this->getState()->isAlive();
	if ( spellbook->find(spellName) == spellbook->end() ) {
		throw NoSuchSpellException();
	}
	spendMana((*spellbook)[spellName]->spellCost);
	(*spellbook)[spellName]->action(target);
	addObservable(target);
}