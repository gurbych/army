#include "Demon.h"

Demon::Demon(const std::string& name, Warlock* master) : Unit(name) {
	this->ability = new DefaultAttack(this, "Basic Melee Attack");
	this->master = master;

	master->setSlave(this);
}

Demon::~Demon() {
    master = NULL;
}
