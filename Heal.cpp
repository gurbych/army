#include "Heal.h"

Heal::Heal(const std::string& spellName, Unit* spellcaster) : Spell(spellName, spellcaster) {}

Heal::~Heal() {}

void Heal::action(Unit* target) {
	if ( this->spellcaster->getState()->isCombatMagician ) {
		target->getState()->addHp(spellPower / 2);
	} else if ( !target->getState()->alive && this->spellcaster->getState()->isPriest ) {
		target->takeMagicDamage(spellPower * 2);
	} else {
		target->getState()->addHp(spellPower);
	}
}
