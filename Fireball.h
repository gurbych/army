#ifndef FIREBALL_H
#define FIREBALL_H

#include "Spell.h"

class Fireball : public Spell {
    public:
        Fireball(const std::string& spellName, Unit* spellcaster);
        virtual ~Fireball();

        void action(Unit* target);
};

#endif
