#ifndef PRIEST_H
#define PRIEST_H

#include "Spellcaster.h"

class Priest : public Spellcaster {
	public:
		Priest(const std::string& name="Priest");
		virtual ~Priest();
};

#endif