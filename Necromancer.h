#ifndef NECROMANCER_H
#define NECROMANCER_H

#include "Spellcaster.h"

class Necromancer : public Spellcaster {
	public:
		Necromancer(const std::string& name="Necromancer");
		virtual ~Necromancer();
		
		virtual void attack(Unit* enemy);
		virtual void takeDamage(int dmg);
		virtual void cast(const std::string& spellName, Unit* target);
};

#endif