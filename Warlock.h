#ifndef WARLOCK_H
#define WARLOCK_H

#include "Spellcaster.h"
#include "Demon.h"

class Demon;

class Warlock : public Spellcaster {
	private:
		Demon* slave;

	public:
		Warlock(const std::string& name="Warlock");
		virtual ~Warlock();

		void summon();
        void freeSlave();
        void setSlave(Demon* demon);
        Demon* demon();
};

#endif