#include "Ability.h"

Ability::Ability(Unit* owner, const std::string& type) {
    this->owner = owner;
    this->type = type;
}

Ability::~Ability() {
    owner = NULL;
}

Unit* Ability::getOwner() const {
    return owner;
}

const std::string& Ability::getType() const {
    return type;
}
