#ifndef BERSERKER_H
#define BERSERKER_H

#include "Unit.h"
#include "DefaultAttack.h"

class Berserker : public Unit {
	public:
        Berserker(const std::string& name="Berserker");
        // virtual ~Berserker();

        virtual void takeMagicDamage(int dmg);
};

#endif