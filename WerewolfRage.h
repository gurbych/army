#ifndef WEREWOLF_RAGE_H
#define WEREWOLF_RAGE_H

#include "Ability.h"

class WerewolfRage : public Ability {
	private:
		int chanceToInfect;

	public:
		WerewolfRage(Unit* owner, const std::string& type);
		virtual ~WerewolfRage();

		virtual void action(Unit* enemy);
};

#endif