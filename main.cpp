#include "Soldier.h"
#include "Vampire.h"
#include "Werewolf.h"
#include "Wizard.h"
#include "Healer.h"
#include "Warlock.h"
#include "Necromancer.h"
#include "Rogue.h"
#include "Berserker.h"
#include "Priest.h"

int main() {
	// Vampire* s1 = new Vampire();
	// Soldier* s1 = new Soldier();
	// Werewolf* w1 = new Werewolf();
	// Wizard* w1 = new Wizard();
	// Warlock* w2 = new Warlock();
	Necromancer* n1 = new Necromancer();

	// s1->attack(w1);
	// s1->attack(s2);
	// s2->attack(s1);
	// w1->changeState();
	// w1->cast("Fireball", s1);
	// w1->cast("Heal", s1);
	// w2->summon();
	// w2->demon()->attack(s1);

	// std::cout << *s1 << std::endl;
	// std::cout << *s2 << std::endl;
	// std::cout << *w1 << std::endl;
	// std::cout << *(w2->demon()) << std::endl;
	std::cout << *n1 << std::endl;

	// delete s1;
	// delete s2;
	// delete w1;
	// delete w2;

	return 0;
}
