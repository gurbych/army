#ifndef HEALER_H
#define HEALER_H

#include "Spellcaster.h"

class Healer : public Spellcaster {
	public:
		Healer(const std::string& name="Healer");
		virtual ~Healer();
};

#endif