#include "Vampire.h"

Vampire::Vampire(const std::string& name) : Unit(name) {
	this->ability = new VampiricAttack(this, "Vampirism");
	this->getState()->turnToUndead();
	this->getState()->isVampire = true;
}

Vampire::~Vampire() {}
